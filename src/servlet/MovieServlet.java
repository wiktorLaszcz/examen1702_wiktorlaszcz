package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Movie;
import presistence.MovieDAO;

@WebServlet({ "/movies", "/movies/*" })
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

       

    public MovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String way = request.getRequestURI();
		String[] p = way.split("/");
		ArrayList <Movie> list = null;
		
		if(way.equals("/Examen1702/movies/index")){
			try{
				 MovieDAO db = new MovieDAO();
				 list = db.all();
				
			}catch (Exception e) {
				// TODO: handle exception
			}
			request.setAttribute("list", list);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movie.jsp");
			dispatcher.forward(request, response);
		}else if(way.equals("/Examen1702/movies/create")){
			request.getRequestDispatcher("/WEB-INF/view/new_movie.jsp").include(request, response);
		}else if(p[4].equals("remember")){
			
			ArrayList<Movie> mlist;
			HttpSession session = request.getSession(true);
			if(session.getAttribute("mlsit") == null){
				session.setAttribute("mlist", new ArrayList<Movie>());
			}
			mlist = (ArrayList<Movie>) session.getAttribute("mlist");
			
			MovieDAO db = new MovieDAO();
			
			mlist.add(db.get(p[3]));
		
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movie.jsp");
			dispatcher.forward(request, response);
		}if(p[4].equals("delete")){
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movie.jsp");
				dispatcher.forward(request, response);
			 MovieDAO db = new MovieDAO();
			 db.delete(p[3]);
		
			
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 if(request.getRequestURI().equals("/Examen1702/movie/store")){
			 
	
				String title = request.getParameter("title");
				String director = request.getParameter("director");
				String year = request.getParameter("year");
				String id = request.getParameter("id");
			
				 MovieDAO db = new MovieDAO();
				 try{
				 db.save(id, title, director, year);
				 }catch (Exception e) {
					// TODO: handle exception
				}
		 }
		 
		 response.sendRedirect("/Examen1702/movies/index");
			
	}

}
