package presistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import model.Movie;

public class MovieDAO {
	
	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/examen1702";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";
    private Connection connection = null;
    
    
    public MovieDAO(){
    	try {
            Class.forName(DB_DRIVER).newInstance();
        } catch (Exception ex) {
            System.out.println("no se ha cargado el driver");
        }
        try {
            connection = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_USER);
            System.out.println("Conectado");

        } catch (SQLException ex) {
            System.out.println("Fallo Conexion");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (connection != null) {
            connection.close();        
        }
    }
    
    public ArrayList <Movie> all() throws SQLException{
        ArrayList<Movie> films = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM movies";

        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Movie film = new Movie();
               
                film.setId(rs.getInt("id"));
                film.setTitle(rs.getString("title"));
                film.setDirector(rs.getString("director"));
                film.setYear(rs.getInt("year"));
                
               
                films.add(film);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
        
        return films;
    }
    
    public void save( String id, String title, String director, String year) throws SQLException{
       
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "INSERT INTO movies (`id`, `title`, `director`, `year`) VALUES ("+id+", "+title+", "+director+", "+year+")";

        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
      
    }
    

    public Movie get(String p) {
    	Movie film = new Movie();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM movies WHERE id = ?";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, p);
            rs = stmt.executeQuery();
      
            while (rs.next()) {
            	 film.setId(rs.getInt("id"));
                 film.setTitle(rs.getString("title"));
                 film.setDirector(rs.getString("director"));
                 film.setYear(rs.getInt("year"));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }             

        return film;
        
    }
    

    public void delete(String p) {
    	Movie film = new Movie();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "DELETE FROM `movies` WHERE id = ?";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, p);
            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }             
    }




}
