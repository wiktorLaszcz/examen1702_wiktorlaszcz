<%@ include file = "header.jsp" %>

 <%
	 List <Movie> list = (List<Movie>) request.getAttribute("list");
     
    if(list == null){
    	list = new ArrayList <Movie>();
    }
	    
	 %>
	 
<div>
  <h2>Peliculas</h2>
  <table>
    <tr>
    	<th>Titulo</th><th>Director</th><th>A�o</th><th> acciones</th>
    </tr>
    <%for(Movie one : list) { %>
    
    <tr>
    	<td>one.getTitle()</td>
    	<td>one.getDirector()</td>
    	<td>one.getYear()</td>
    	<td>
    		<a  href="movie/<%= one.getId() %>">ver -</a>
    		<a href="movie/<%= one.getId() %>/delete/"> borrar -</a>
    		<a href="movie/<%= one.getId() %>/remember"> recordar</a>
    	</td>
    </tr>
    
    <%}%>
    
  </table>
</div>

<%@ include file = "footer.jsp" %>